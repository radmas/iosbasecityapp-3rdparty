//
//  Logger.swift
//  CasiopeaDEV
//
//  Created by Ángel González Abad on 01/07/2019.
//  Copyright © 2019 Radmas. All rights reserved.
//

import CocoaLumberjack
import iOSBase

internal class Logger: LoggerDelegate_Cpt {
    private let memCacheSize: Int = 20 * 1_024 * 1_024
    private let diskCacheSize: Int = 100 * 1_024 * 1_024
    private let rollingFrecuencySec: TimeInterval = 72 * 60 * 60  // 72 hours
    private let maxLogFileSize: UInt64 = 1_024 * 1_024 //1 MB
    private var logLevel: DDLogLevel

    init() {
        self.logLevel = Logger.setDefaultLogLevel()
        self.prepareLoggers()
        self.printStartLog()
    }

    private static func setDefaultLogLevel() -> DDLogLevel {
        #if RELEASE
        return .info
        #else
        return .all
        #endif
    }

    private func prepareLoggers() {
        self.addOSLogger()
        let cache: URLCache = URLCache(memoryCapacity: self.memCacheSize, diskCapacity: self.diskCacheSize, diskPath: nil)
        URLCache.shared = cache
        let fileLogger: DDFileLogger = DDFileLogger() // File Logger
        fileLogger.rollingFrequency = self.rollingFrecuencySec
        fileLogger.maximumFileSize = self.maxLogFileSize
        fileLogger.logFileManager.maximumNumberOfLogFiles = 1
        DDLog.add(fileLogger)
    }

    private func addOSLogger() {
        #if DEBUG
        DDLog.add(DDOSLogger.sharedInstance) // os_log
        #endif
    }

    private func printStartLog() {
        info("\n")
        info("////////////////////////////////////////////////")
        info("/////        START      OF   LOGS         //////")
        info("////////////////////////////////////////////////")
        info("\n")
    }

    func verbose(_ text: String) {
        DDLogVerbose(text, level: self.logLevel)
    }

    func debug(_ text: String) {
        DDLogDebug(text, level: self.logLevel)
    }

    func info(_ text: String) {
        DDLogInfo(text, level: self.logLevel)
    }

    func warn(_ text: String) {
        DDLogWarn(text, level: self.logLevel)
    }

    func error(_ text: String) {
        DDLogError(text, level: self.logLevel)
    }
}
