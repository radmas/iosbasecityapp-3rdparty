//
//  AppConfigCaller.swift
//  ExternalApp
//
//  Created by Ángel González Abad on 08/04/2019.
//  Copyright © 2019 ExternalEnterprise. All rights reserved.
//

import Foundation
import iOSBase

internal class AppConfigCaller: AppConfigurationDelegate_Cpt {
    private let configDictionary = Bundle.main.infoDictionary?["ApplicationConfiguration"] as? [String: AnyObject]

	func getAppColor() -> String? {
		self.configDictionary?["app_color"] as? String
	}
	
	func getCustomTrackingMessage() -> String? {
		self.configDictionary?["custom_tracking_alert_message"] as? String ?? nil
	}
	
	func getWithDisclaimer_Cpt() -> Bool {
		false
	}
	
	func getTrackingDisclaimerVisibility() -> Bool {
		false
	}
	
    func getEmailApp_Cpt() -> String? {
        self.configDictionary?["email_app"] as? String
    }

    func getiOSVersion_Cpt() -> String? {
        UIDevice.current.systemVersion
    }

    func getApplicationLanguages_Cpt() -> [String] {
        self.configDictionary?["languages"] as? [String] ?? []
    }

    func getJurisdictionEmail_Cpt() -> String? {
        self.configDictionary?["email_jurisdiction"] as? String
    }

    func getApplicationId_Cpt() -> String? {
        self.configDictionary?["app_id"] as? String
    }

    func getJurisdictionId_Cpt() -> String? {
        self.configDictionary?["jurisdiction_id"] as? String
    }

    func getAppVersion_Cpt() -> String? {
        Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    func getAppKey_Cpt() -> String? {
        self.configDictionary?["app_key"] as? String
    }

    func getBaseURL_Cpt() -> String? {
        self.configDictionary?["base_url"] as? String
    }

    func getClientId_Cpt() -> String? {
        self.configDictionary?["client_id"] as? String
    }

    func getConfigurationElement_Cpt(named: String) -> String? {
        self.configDictionary?[named] as? String
    }

    func getDefaultConfigurationElement_Cpt(named: String) -> String? {
        self.configDictionary?["default"]?[named] as? String
    }

    func getEmergencyNames() -> [String]? {
        configDictionary?["emergency_names"] as? [String]
    }

    func getEmergencyNumbers() -> [String]? {
        configDictionary?["emergency_numbers"]as? [String]
    }

    func getLoginType_Cpt() -> String? {
        self.configDictionary?["login_type"] as? String
    }

    func getLoginEndpoint_Cpt() -> String? {
        self.configDictionary?["login_endpoint"] as? String
    }

    func getRefreshTokenEndpoint_Cpt() -> String? {
        configDictionary?["refresh_token_endpoint"] as? String
    }

    func getStoreData_Cpt() -> [String: String] {
        self.configDictionary?["store"] as? [String: String] ?? [:]
    }

    func getAnalyticsURL_Cpt() -> String? {
        self.configDictionary?["analytics_url"] as? String
    }

    func getAnalyticsExcludeFlagged_Cpt() -> Bool {
        self.configDictionary?["analytics_exclude_flagged"] as? Bool ?? false
    }

    func getWhitelistRequiredFlag_Cpt() -> Bool? {
        self.configDictionary?["requires_whitelisting"] as? Bool
    }

    func getTutorialsEnabled_Cpt() -> Bool? {
        guard let tutorialsSettings: [String: Any] = self.configDictionary?["tutorials"] as? [String: Any] else {
            return nil
        }
        return tutorialsSettings["enabled"] as? Bool
    }

    func getExcludedTutorials_Cpt() -> [String] {
        guard let tutorialsSettings: [String: Any] = self.configDictionary?["tutorials"] as? [String: Any] else {
            return []
        }
        return tutorialsSettings["excluded"] as? [String] ?? []
    }

    func getSupportData_Cpt() -> [String: String] {
        self.configDictionary?["support"] as? [String: String] ?? [:]
    }

    func getEnvironmentName() -> String? {
        configDictionary?["environment_name"] as? String
    }

    func getWaterMarks_Cpt() -> Bool? {
        configDictionary?["water_mark"] as? Bool
    }
}
