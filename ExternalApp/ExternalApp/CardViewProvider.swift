//
//  ExternalModuleDependencyInyector.swift
//  iOSBaseSDK
//
//  Created by Jose on 13/02/2020.
//  Copyright © 2020 Radmas. All rights reserved.
//

import MTXThirdPartyCard
internal enum ExternalModuleDependencyInyector {
    static func configureExternalDependencies() {
        MTXThirdPartyCardModuleDependencies().configureDependencies()
    }
}
