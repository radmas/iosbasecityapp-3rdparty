//
//  AppDelegate.swift
//  ExternalApp
//
//  Created by Ángel González Abad on 08/04/2019.
//  Copyright © 2019 ExternalEnterprise. All rights reserved.
//

import CocoaLumberjack
import iOSBase
import KeychainAccess
import UIKit
import UserNotifications

internal final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    lazy var appConfigCaller: AppConfigCaller = AppConfigCaller()
    private weak var screen: UIView?
    private lazy var logger: Logger = Logger()
	
	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		DispatchQueue.global(qos: .userInteractive).async {
			self.userInteractiveQueueLaunch(application, launchOptions: launchOptions)
		}
		self.mainQueueLaunch()
		return true
	}
    
    private func mainQueueLaunch() {
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        self.prepareRootViewController()
        ExternalModuleDependencyInyector.configureExternalDependencies()
    }

    private func userInteractiveQueueLaunch(_ application: UIApplication, launchOptions: [ UIApplication.LaunchOptionsKey: Any ]?) {
        self.prepareLogManager()
    }
    
    private func prepareRootViewController() {
        self.prepareAppConfigurationDelegate_Cpt()
		let uiWindow: UIWindow = UIWindow(frame: UIScreen.main.bounds)
		self.window = uiWindow
		uiWindow.backgroundColor = UIColor.white
		uiWindow.makeKeyAndVisible()
    }

    func prepareLogManager() {
        BaseSDK_Cpt.sharedInstance.getLoggerSDK_Cpt().setDelegate_Cpt(delegate: self.logger)
    }

    private func updateNotificationsReceived() {
        BaseSDK_Cpt.sharedInstance.getNotificationSDK_Cpt().getNotificationDelegate_Cpt()?.addSavedNotificationsFromCenter()
        BaseSDK_Cpt.sharedInstance.getNotificationSDK_Cpt().getNotificationDelegate_Cpt()?.updateNotReadBadge()
    }

    private func prepareAppConfigurationDelegate_Cpt() {
        BaseSDK_Cpt.sharedInstance.getAppConfigurationSDK_Cpt().setDelegate_Cpt(delegate: self.appConfigCaller)
    }

    private func addPrivacyLayer_Cpt() {
        screen = snapshotView_Cpt()
        guard let screen = screen else { return }
        addBlur_Cpt(to: screen)
        addLoginLogo_Cpt(to: screen)
        setToWindow_Cpt(view: screen)
    }

    private func snapshotView_Cpt() -> UIView {
        return UIScreen.main.snapshotView(afterScreenUpdates: false)
    }

    private func addBlur_Cpt(to view: UIView) {
        let blurEffect = UIBlurEffect(style: .regular)
        let blurBackground = UIVisualEffectView(effect: blurEffect)
        blurBackground.frame = view.frame
        view.addSubview(blurBackground)
    }

    private func addLoginLogo_Cpt(to view: UIView) {
        let logoImage = UIImage(named: "loginLogo", in: Bundle.main, compatibleWith: nil)
        let logoImageView = UIImageView(image: logoImage)
        logoImageView.tintColor = UIColor(hexadecimalString: BaseSDK_Cpt.sharedInstance
            .getConfigurationSDK_Cpt().getApplicationColor_Cpt())
        view.addSubview(logoImageView)
        logoImageView.center = view.center
    }

    private func setToWindow_Cpt(view: UIView) {
        window?.addSubview(view)
    }

    private func removePrivacyLayer_Cpt() {
        screen?.removeFromSuperview()
    }

    internal func application(_ application: UIApplication,
                              didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let store = Keychain()
        let tokenStringified = deviceToken.map({ String(format: "%02.2hhx", $0) }).joined()
        store["notification_account_code"] = tokenStringified
    }

    internal func application(_ application: UIApplication,
                              didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Remote notification support is unavailable due to error: \(error.localizedDescription)")
    }

    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        updateNotificationsReceived()
        completionHandler(.newData)
    }

    internal func applicationWillResignActive(_ application: UIApplication) {
        /* Sent when the application is about to move from active to inactive state.
         * This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message)
         * or when the user quits the application and it begins the transition to the background state.
         *
         * Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks.
         * Games should use this method to pause the game.
         */
        self.addPrivacyLayer_Cpt()
    }

    internal func applicationDidEnterBackground(_ application: UIApplication) {
        /* Use this method to release shared resources, save user data, invalidate timers,
         * and store enough application state information to restore your application to
         * its current state in case it is terminated later.
         *
         * If your application supports background execution,
         * this method is called instead of applicationWillTerminate: when the user quits.
         */
    }

    internal func applicationWillEnterForeground(_ application: UIApplication) {
        /* Called as part of the transition from the background to the active state;
         * here you can undo many of the changes made on entering the background.
         */
        updateNotificationsReceived()
    }

    internal func applicationDidBecomeActive(_ application: UIApplication) {
        /* Restart any tasks that were paused (or not yet started) while the application was inactive.
         * If the application was previously in the background, optionally refresh the user interface.
         */
        updateNotificationsReceived()
        self.removePrivacyLayer_Cpt()
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadReleases"), object: nil)
        }
    }

    internal func applicationWillTerminate(_ application: UIApplication) {
        /* Called when the application is about to terminate.
         * Save data if appropriate. See also applicationDidEnterBackground:.
         */
        BaseSDK_Cpt.sharedInstance.getSessionSDK_Cpt().deleteOptionalUpdateRead_Cpt()
    }
}
