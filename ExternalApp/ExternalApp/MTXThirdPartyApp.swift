//
//  MTXThirdPartyApp.swift
//  MTXThirdPartyApp
//
//  Created by Martin Bueno Martin on 9/12/21.
//  Copyright © 2021 Radmas. All rights reserved.
//

import SwiftUI
import iOSBase

@main
struct iOSBaseSDKApp: App {
	@UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
	
	var body: some Scene {
		WindowGroup {
			BaseSDK_Cpt.sharedInstance.getBaseRootVC_Cpt().getRootView().self
		}
	}
}

