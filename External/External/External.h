//
//  External.h
//  External
//
//  Created by Ángel González Abad on 08/04/2019.
//  Copyright © 2019 ExternalEnterprise. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for External.
FOUNDATION_EXPORT double ExternalVersionNumber;

//! Project version string for External.
FOUNDATION_EXPORT const unsigned char ExternalVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <External/PublicHeader.h>


