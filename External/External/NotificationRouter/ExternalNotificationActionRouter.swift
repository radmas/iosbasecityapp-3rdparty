//
//  ExternalNotificationActionRouter.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 28/6/18.
//  Copyright © 2018 Radmas. All rights reserved.
//

import iOSBase
import Viperit

public final class ExternalNotificationActionRouter_Cpt: NotificationActionRouterProtocol {
    public func prepareNotificationObservers() {}
    
    public func getNotificationType_Cpt() -> Int {
        return -1
    }

    public func prepareNotificationObservers_Cpt() {}

    public func executeActionOnDisplay_Cpt(userInfo: [AnyHashable : Any]) {}

    public func executeAction_Cpt(userInfo: [AnyHashable : Any]) {}

    public func executeActionFromNotifMailbox_Cpt(userInfo: [AnyHashable : Any]) {}
}
