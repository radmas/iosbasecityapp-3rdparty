//
//  ExternalCardSwiftUI.swift
//  MTXThirdPartyCard
//
//  Created by Martin Bueno Martin on 9/12/21.
//  Copyright © 2021 Radmas. All rights reserved.
//

import SwiftUI
import iOSBase

struct ExternalCardSwiftUI: View, BaseCardSwiftUIProtocol {
	private let cardEntity: CardEntity
	
	init(cardEntity: CardEntity) {
		self.cardEntity = cardEntity
	}
	
    var body: some View {
		ZStack {
			Color.gray
			Text("Dummy SiwftUI card")
				.font(.title)
				.fontWeight(.bold)
		}
		.frame(height: 200)
	}
    
    func getCardView() -> AnyView {
        AnyView(self)
    }
    
    func getCardSettingsAction_Cpt() -> [CardOptionEntity] {
        []
    }
}

