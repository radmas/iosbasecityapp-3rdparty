//
//  ExampleRepository.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 18/6/18.
//  Copyright © 2018 Radmas. All rights reserved.
//
import iOSBase

internal final class ExampleRepository_Cpt: Repository {
    typealias DiskDataSource = ExampleDiskDataSource_Cpt
    typealias APIDataSource = ExampleAPIDataSource_Cpt

    func getDiskDataSource() -> ExampleDiskDataSource_Cpt {
        return ExampleDiskDataSource_Cpt()
    }

    func getAPIDataSource() -> ExampleAPIDataSource_Cpt {
        return ExampleAPIDataSource_Cpt(provider: ServerProvider())
    }
}
