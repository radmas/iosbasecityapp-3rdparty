//
//  GreetingView.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 10/04/2019.
//  Copyright © 2019 Radmas. All rights reserved.
//

import UIKit
import Viperit

// MARK: - Public Interface Protocol
protocol GreetingViewInterface {
    func prepareView()
}

// MARK: GreetingView Class
final class GreetingView: UserInterface {
}

// MARK: - Public interface
extension GreetingView: GreetingViewInterface {
    func prepareView() {
        self.navigationController?.tintBarWithMainColor()
    }
}

// swiftlint:disable force_cast
// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension GreetingView {
    var presenter: GreetingPresenter {
        return _presenter as! GreetingPresenter
    }
    var displayData: GreetingDisplayData {
        return _displayData as! GreetingDisplayData
    }
}
