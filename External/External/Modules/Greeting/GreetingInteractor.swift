//
//  GreetingInteractor.swift
//  MTXThirdPartyCard
//
//  Created by Ángel González Abad on 10/04/2019.
//  Copyright © 2019 Radmas. All rights reserved.
//

import Foundation
import Viperit

class GreetingInteractor: Interactor {
}

// swiftlint:disable force_cast
// MARK: - VIPER COMPONENTS API (Auto-generated code)
private extension GreetingInteractor {
    var presenter: GreetingPresenter {
        return _presenter as! GreetingPresenter
    }
}
