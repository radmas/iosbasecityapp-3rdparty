//
//  ExternalCardViewController.swift
//  MTXThirdPartyCard
//
//  Created by Jose Tapia on 9/12/21.
//  Copyright © 2021 Radmas. All rights reserved.
//

import UIKit
import iOSBase

class ExternalCardViewController: BaseCardUIkit_Cpt {

    public var cardSettingsAction: [UIAlertAction] = []
    private let notificationName = "ExternalNotification"
    
    @IBOutlet weak var defaultImageView: UIImageView!
    @IBOutlet weak var defaultTextLabel: UILabel!
    
    init() {
        super.init(nibName: "ExternalCardViewController", bundle: Bundle(for: type(of: self)))
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.prepareView()
        updateDefaultLabel()
    }


    override public func getCardViewCellHeight_Cpt() -> CGFloat {
        return 200
    }

    override public func getCardViewIcon_Cpt() -> UIImage? {
        return UIImage(named: "cardIcon", in: Bundle(for: type(of: self)), compatibleWith: nil)
    }

    public func addModule_Cpt() {}
    public func getAddCardImage_Cpt() -> UIImage? {
        return UIImage(named: "addthirdPartyCard", in: Bundle(for: type(of: self)), compatibleWith: nil)
    }

    internal func prepareView() {
        var tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDetail_Cpt))
        defaultImageView.addGestureRecognizer(tapGesture)
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showDetail_Cpt))
        defaultTextLabel.addGestureRecognizer(tapGesture)
    }

    @objc
    private func showDetail_Cpt() {
        let module = ThirdPartyCardModules.Greeting.build(bundle: Bundle(for: type(of: self)), deviceType: .phone)
        module.view.viewController.hidesBottomBarWhenPushed = true
        if let navController = self.navigationController, let vc = navController.visibleViewController {
            navController.navigationBar.prefersLargeTitles = false
            navController.isNavigationBarHidden = false
            module.router.show(from: vc, embedInNavController: false, setupData: nil)
        }
    }

    func updateDefaultLabel() {
        var message: String = "El usuario no ha iniciado sesión"
        if BaseSDK_Cpt.sharedInstance.getSessionSDK_Cpt().isLoggedIn_Cpt() {
            if let email = BaseSDK_Cpt.sharedInstance.getUserSDK_Cpt().getCurrentUser_Cpt()?.getEmail() {
                message = "El email del usuario es: \(email)"
            }else {
                message = "El email no ha podido ser encontrado"
            }
        }
        defaultTextLabel.text = message
    }
}
