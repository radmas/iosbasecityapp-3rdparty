//
//  MTXThirdPartyCardModuleDependencies.swift
//  MTXThirdPartyCard
//
//  Created by Jose on 14/02/2020.
//  Copyright © 2020 Radmas. All rights reserved.
//

import iOSBase
public final class MTXThirdPartyCardModuleDependencies: ExternalModuleDependencyInjectorProtocol {
    public init(){}
    public func configureDependencies() {
        BaseSDK_Cpt.sharedInstance.getCardsSDK_Cpt().setCardProvider_Cpt(moduleType: "trash_card", cardViewProvider: ExternalCardProvider())
//		BaseSDK_Cpt.sharedInstance.getCardsSDK_Cpt().setCardProvider_Cpt(moduleType: "trash_card", cardViewProvider: ExternalCardSwiftUIProvider())
    }
}
