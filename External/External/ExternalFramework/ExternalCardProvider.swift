//
//  ExternalCardProvider.swift
//  MTXThirdPartyCard
//
//  Created by Jose on 14/02/2020.
//  Copyright © 2020 Radmas. All rights reserved.
//

import iOSBase
public class ExternalCardProvider: CardViewProviderProtocol_Cpt {
	public func getSwiftUIView(cardEntity: CardEntity) -> Any? {
		CardRepresentation(cardView: ExternalCardViewController())
	}
}
