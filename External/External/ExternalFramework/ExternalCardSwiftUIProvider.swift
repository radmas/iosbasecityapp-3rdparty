//
//  ExternalCardSwiftUIProvider.swift
//  MTXThirdPartyCard
//
//  Created by Martin Bueno Martin on 9/12/21.
//  Copyright © 2021 Radmas. All rights reserved.
//

import iOSBase
import SwiftUI
public class ExternalCardSwiftUIProvider: CardViewProviderProtocol_Cpt {
	public func getSwiftUIView(cardEntity: CardEntity) -> Any? {
        ExternalCardSwiftUI(cardEntity: cardEntity)
	}
}
