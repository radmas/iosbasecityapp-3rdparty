
  

# Creación de un módulo para IYX - MTX  (iOS)

## Requisitos mínimos:

Un módulo para MTX requiere que sea compatible con iOS 15.0.1 dado que nuestro SDK utiliza [SwiftUI](https://developer.apple.com/xcode/swiftui/)

## Preparación

Para la creación de un nuevo módulo se proporciona un espacio de trabajo preparado para ayudarlo en el desarrollo **3rdPartyWorkspace.xcworkspace** que está en el [repositorio](https://bitbucket.org/radmas/iosbasecityapp-3rdparty/src/master/).

*Nota: Las credenciales de login y el servidor, serán proporcionados. Una vez obtenido las credenciales del entorno deseado se deben actualizar los parámetros el client_id y base_url del archivo *_info.plist_* situado en MTXThirdPartyApp/info .*

Cuando clonamos el repositorio y debemos ejecutar los siguientes comandos:

```
cd iosbasecityapp-3rdparty
pod install
```
Estos comandos son necesarios para instalar las dependencias del SDK , nuestro SDK utiliza **CococaPods** como gestor de dependencias. Tu puedes instalar CocoaPods siguiendo las instrucciones en  su [sitio web](https://cocoapods.org/).


## MTX XCode workspace
Abre el archivo ```3rdPartyWorkspace.xcworkspace```, y en  la pestaña de navegación podemos ver :

- **MTXThirdPartyApp:** Es el proyecto de prueba que inicializa nuestro SDK.

- **MTXThirdPartyCard**:  Es un ejemplo de módulo para MTX.

- **Pods**: Un proyecto generado por CocoaPods que tiene todas las dependencias agregadas a través de esa aplicación.

 
## Como crear un nuevo módulo

Primero, dentro del espacio de trabajo provisto, cree un nuevo proyecto en XCode.

```
File -> New -> Project...
```

Después de esto, en **"Choose a template..."**, selecciona **"Cocoa Touch Framework"**, rellena los campos requeridos para su creación y añadelo al workspace.


### Configura el módulo para MTX

Dentro de nuestro proyecto, necesitamos configurarlo para asegurar la compatibilidad con los desarrollos de MTX.



  

 1. Cambia la propiedad **deployment target** version a **iOS 15.0.1**.
 2. Cambia la versión de swift para que coincida con el proyecto base. En estos momentos la versión actual de Swift es 5. ve a la configuración del target, en la pestaña **build settings**  y busca ```SWIFT_VERSION```.
 3. Ve a la pestaña **general** y añade el framework iOSBase desde la sección **Linked Frameworks and Libraries** 

Finalmente, modifica el ```Podfile``` para añadir las librerías externas de tu módulo, vuelve al terminal  y ejecuta ```pod install```

#### Podfile example
```
platform :ios, '15.0.1'
def mtxBasePods
pod 'SDWebImage'
pod 'Alamofire'
pod 'KeychainAccess'
pod 'XMLDictionary'
pod 'SwiftMessages'
pod 'CocoaLumberjack/Swift'
pod 'OpalImagePicker'
pod 'EFMarkdown'
end

target 'MTXThirdPartyApp'  do
use_frameworks!
workspace '3rdPartyWorkspace.xcworkspace'
project 'ExternalApp/MTXThirdPartyApp.xcodeproj'
mtxBasePods
end

target 'MTXThirdPartyCard'  do
use_frameworks!
workspace '3rdPartyWorkspace.xcworkspace'
project 'External/MTXThirdPartyCard.xcodeproj'
mtxBasePods
end

# Pods project optimizations.# Pods project optimizations.
post_install do |installer|
    installer.pods_project.targets.each do |target|
        target.build_configurations.each do |config|
            config.build_settings['CLANG_WARN_QUOTED_INCLUDE_IN_FRAMEWORK_HEADER'] = 'NO'
            config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
            if config.name == 'Debug'
                config.build_settings['OTHER_SWIFT_FLAGS'] = ['$(inherited)', '-Onone']
                config.build_settings['SWIFT_OPTIMIZATION_LEVEL'] = '-Owholemodule'
            end
        end
    end
end
```
## Crea una tarjeta

Ahora vamos a crear una tarjeta, primero creamos un nuevo archivo donde alojar la tarjeta, este archivo debe ser una vista **SwiftUI**

```
File -> New -> File -> SwiftUI View
```
En nuestro ejemplo vamos crearnos la clase **ExternalCardSwiftUI.Swift**  con un ejemplo de una vista básica.
```swift

import SwiftUI
import iOSBase

struct  ExternalCardSwiftUI: View, BaseCardSwiftUIProtocol {
    private let cardEntity: CardEntity

    init(cardEntity: CardEntity) {
        self.cardEntity = cardEntity
    }

    var body: some View {
        ZStack {
        Color.gray
        Text("Dummy SiwftUI card")
            .font(.title)
            .fontWeight(.bold)
        }
        .frame(height: 200)
    }

    func  getCardView() -> AnyView {
        AnyView(self)
    }

    func getCardSettingsAction_Cpt() -> [CardOptionEntity] {
        []
    }
}
```

### Registra tus dependencias en la base:

Vamos a crearnos una clase para habilitar nuestras dependencias, esta debe implementar el protocolo **ExternalModuleDependencyInjectorProtocol**. Esta clase debe ser utilizada para registrar todas las dependencias del módulo hacia la base:

```swift
import iOSBase

public final class MTXThirdPartyCardModuleDependencies: ExternalModuleDependencyInjectorProtocol {
    public  init(){}
    public  func  configureDependencies() {
        BaseSDK_Cpt.sharedInstance.getCardsSDK_Cpt().setCardProvider_Cpt(
	        moduleType: "twitter_card",
	        cardViewProvider: ExternalCardProvider()
        )
    }
}
```
En el ejemplo se accede al SDK de tarjetas para registrar la tarjeta de ejemplo, para ello se debe obtener un identificador, en este ejemplo es  **"twitter_card"** y el proveedor de tarjetas es **"ExternalCardProvider"**, este proveedor implementa el protocolo **CardViewProviderProtocol_Cpt**


```swift
import iOSBase

public class ExternalCardProvider: CardViewProviderProtocol_Cpt {
    public func getSwiftUIView(cardEntity: CardEntity) -> Any? {
        ExternalCardSwiftUI(cardEntity: cardEntity)
    }
}
```
Para crear una mostrar una tarjeta en nuestra home tenemos que devolver un objeto que implemente el protocolo **BaseCardSwiftUIProtocol**. 

### Añadir nuestro módulo a la aplicación de prueba
Dentro del target MTXThirdPartyApp, necesitamos añadir nuestro módulo desde la sección **Embedded Binaries** dentro de la pestaña **General**.

Una vez añadido el módulo debemos registrar nuestras dependencias en el archivo **ExternalModuleDependencyInyector**

 
```swift
import MTXThirdPartyCard

internal  enum  ExternalModuleDependencyInyector {
    static  func  configureExternalDependencies() {
        MTXThirdPartyCardModuleDependencies().configureDependencies()
    }
}
```
Finalmente, compila y ejecuta la aplicación de prueba. Deberás ver una tarjeta con la vista que hayas definido


Ahora, tiene un buen espacio para comenzar con su nuevo módulo.

## Posibles problemas

### iOSBase module not found
Comprueba  **Framework search path** dentro del target de la tarjeta en la pestaña **build settings**.  Debe tener una ruta agregada a la carpeta de la aplicación de prueba. En el ejemplo proporcionado en esta guía es "MTXThirdPartyCard", para solucionar el error necesitamos poner esta ruta de búsqueda.
```
$(PROJECT_DIR)/../ExternalApp/Frameworks
```

  

Donde:
- ```$(PROJECT_DIR)``` es la ruta donde se encuentra la tarjeta.
- ```..``` se refiere a la carpeta principal.
- ```ExternalApp``` es la carpeta donde se encuentra el proyecto de la aplicación de prueba
- ```Frameworks``` es la carpeta donde se colocan los módulos externos.
**No eliminar $(inherited)**
