//
//  MyOwnCardTVCell.swift
//  MyOwnCard
//
//  Created by Ángel González Abad on 11/04/2019.
//  Copyright © 2019 ExampleOrganization. All rights reserved.
//

import iOSBase
import UIKit

public final class MyOwnCardTVCell:  BaseCardView, AddModuleProtocol {
    override public var className: String {
        return "MyOwnCardTVCell"
    }

    override public var cellReuseIdentifier: String? {
        return "MyOwnCardTVCellReuseIdentifier"
    }

    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override public func getCardViewCellHeight() -> CGFloat {
        return 260
    }

    override public func getCardViewIcon() -> UIImage? {
        return nil
    }

    public func addModule() {}
    public func getAddCardImage() -> UIImage? {
        return nil
    }
}
